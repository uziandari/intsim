import React, { Component } from 'react';

export default class FilterUser extends Component {
  
  //pass to state function in TransactionsPage
  onUserChange(e) {
    this.props.changeUser(e.target.value);
  }

  render() {
    return (
      <div>
        <input type='number' name='customerIDFilter' placeholder='Enter Customer ID...' value={this.props.customerID} onChange={(e) => this.onUserChange(e)} />
      </div>
    );
  }
}

