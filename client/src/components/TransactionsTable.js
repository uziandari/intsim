import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

const TransactionsTable = (props) => (
  <div className="transactions-table">
    <MuiThemeProvider>
      <Table multiSelectable={true}>
        <TableHeader displaySelectAll={true}>
          <TableRow>
            <TableHeaderColumn>Customer</TableHeaderColumn>
            <TableHeaderColumn>Transaction #</TableHeaderColumn>
            <TableHeaderColumn>SKU</TableHeaderColumn>
            <TableHeaderColumn>Description</TableHeaderColumn>
            <TableHeaderColumn>Qty Ordered</TableHeaderColumn>
            <TableHeaderColumn>Date</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={true}>
          {props.transactionsList.map((row, index) => (
            <TableRow key={index}>
              <TableRowColumn>{row.customer_id} &ndash; {row.last_name}&#44; {row.first_name}</TableRowColumn>
              <TableRowColumn>{row.transaction_id}</TableRowColumn>
              <TableRowColumn>{row.product_name}</TableRowColumn>
              <TableRowColumn>{row.product_description}</TableRowColumn>
              <TableRowColumn>{row.qty_ordered}</TableRowColumn>
              <TableRowColumn>{row.transaction_date}</TableRowColumn> {/* todo --momentJS for proper format */}
            </TableRow>
          ))}
        </TableBody>  
      </Table>
      {/* To add - pagination */}
    </MuiThemeProvider>
  </div>
);

export default TransactionsTable;