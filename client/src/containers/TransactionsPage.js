import React, { Component } from 'react';

import FilterUser from '../components/FilterUser';
import TransactionsTable from '../components/TransactionsTable';

//material-ui
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import ActionPrint from 'material-ui/svg-icons/action/print';

//assign base express url
const apiBaseUrl = "http://localhost:3000/api/";

export default class TransactionsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transactions: [],
      customersList: [],
      customerID: ''
    };
  };
  
  async getTransactionsFromApi(id) {
    //gather all users
    if (!id) {
      try {
        let response = await fetch(apiBaseUrl + 'transactions');
        let responseJson = await response.json();
        let users = await fetch(apiBaseUrl + 'customers')
        let userJson = await users.json();
        this.setState({
          transactions: responseJson,
          customersList: userJson
        });
      } catch(error) {
        console.error(error);
      }
    }
    else {
      try {
        let response = await fetch(apiBaseUrl + 'transactions/' + this.state.customerID);
        let responseJson = await response.json();
        this.setState({
          transactions: responseJson,
        });
      } catch(error) {
        console.error(error);
      }
    }
  }

  //filter to specific user
  async handleIDChange(user) {
    await this.setState({
      customerID: user
    });
    this.getTransactionsFromApi(this.state.customerID);
  }

  printTable() {
    console.log('printing')
    window.print();
  }

  componentDidMount() {
    this.getTransactionsFromApi(this.state.customerID);
  }

  render() {
    return (
      <div className="transactions-container">
        <h2>Transactions</h2>
        <FilterUser customerID={this.state.customerID} changeUser={(e) => this.handleIDChange(e)}/>
        <button onClick={() => this.handleIDChange('')}>Clear</button>
        {/* Don't render if transactions result is empty */}
        {/* Todo -- wrap component in a Loading container */}
        {this.state.transactions.length > 0 ?
          <div className="table-print">
          {/* Print Button */}
          <MuiThemeProvider>
            <FlatButton
              icon={<ActionPrint />}
              primary={true}
              onClick={this.printTable}
            />  
          </MuiThemeProvider>
          <TransactionsTable transactionsList={this.state.transactions} />
          </div> : null
        }
      </div>
    );
  }
}
