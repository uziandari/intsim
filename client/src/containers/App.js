import React, { Component } from 'react';

import TransactionsPage from './TransactionsPage';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        {/*Nav*/}
        <TransactionsPage />
      </div>
    );
  }
}
