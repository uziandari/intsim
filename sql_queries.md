##Table Structure:

```sql

      CREATE TABLE  IF NOT EXISTS `customers` (
      `customer_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
      `first_name` VARCHAR(35) NOT NULL,
      `last_name` VARCHAR(35) NOT NULL,
      `age` TINYINT(3) UNSIGNED NULL DEFAULT NULL,
      `last_login` DATETIME NULL DEFAULT NULL,
      `email` VARCHAR(254) NOT NULL,
      `account_balance` DECIMAL(19,4) NOT NULL DEFAULT '0.0000',
      PRIMARY KEY (`customer_id`),
      INDEX `first_name` (`first_name`),
      INDEX `last_name` (`last_name`),
      INDEX `email` (`email`),
      INDEX `account_balance` (`account_balance`)
      )
      COLLATE='utf8_general_ci'
      ENGINE=InnoDB
      AUTO_INCREMENT=1
      ;



      CREATE TABLE  IF NOT EXISTS `transactions` (
      `transaction_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
      `transaction_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
      `customer_id` INT(10) UNSIGNED NOT NULL,
      `transaction_amount` DECIMAL(19,4) NOT NULL,
      PRIMARY KEY (`transaction_id`),
      UNIQUE INDEX `transactions_id_UNIQUE` (`transaction_id`),
      INDEX `transaction_date` (`transaction_date`),
      INDEX `transaction_amount` (`transaction_amount`),
      INDEX `customer_id` (`customer_id`),
      FOREIGN KEY `customer_id` (`customer_id`)
      REFERENCES `customers`(`customer_id`)
      ON UPDATE NO ACTION
      ON DELETE CASCADE
      )
      COLLATE='utf8_general_ci'
      ENGINE=InnoDB
      AUTO_INCREMENT=1
      ;

```


### 1.	Can you provide example queries that insert sample customer and transaction records into the database?

```sql

      INSERT INTO `database`.customers (first_name, last_name, age, email)
      VALUES ('John', 'Doe', 18, 'j.doe@gmail.com');

      INSERT INTO `database`.customers (first_name, last_name, age, email, account_balance)
      VALUES ('Jane', 'Smith', 40, 'janeS@yahoo.com', 5000);

      INSERT INTO `database`.transactions (customer_id, description, transaction_amount)
      VALUES (1, 'Company Sale', 15);

      INSERT INTO `database`.transactions (customer_id, description, transaction_amount)
      VALUES (1, 'Company Sale', 500.000000);

```

### 2.	Joe said he will want to see which transactions came from a particular customer.  How would you do that?

```sql

      SELECT *
      FROM `database`.transactions
      WHERE customer_id = 1
      ;

```

### 3.	Joe wants to filter the transactions by date.  How would you modify the previous query?

```sql

      SELECT *
      FROM `database`.transactions
      WHERE DATE(transaction_date) = '2017-8-28'
      ;

```

### 4.	Joe wants to find all transactions less than $40.00 but greater than $20.00.  How would you write the query?

```sql

      SELECT *
      FROM `database`.transactions
      WHERE transaction_amount < 40
      AND transaction_amount > 20
      ;

```

### 5.	Joe wants to find all customers that have a gmail email address.  Can you provide a query that would do that?

```sql

      SELECT *
      FROM `database`.customers
      WHERE email LIKE '%@gmail.com'
      ;

```

### 6.	Joe has entered several transactions after the ones you added for testing at this point.  He decided he wants to change the Description of some of them from “Company Sale” to “Widget Transaction”.  Can you write a query that would change that for him?

```sql

      UPDATE `database`.transactions
      SET description = 'Widget Transaction'
      WHERE description = 'Company Sale'
      ;

```

### 7.	Joe created a lot of test customers in the database and he wants to clean it up before the next phase of testing.  Can you write a query(s) that would delete all customer records that have “Test” as the first name and all associated transactions?

```sql

      #'ON DELETE CASCADE' will delete records within the 'transactions' table
      DELETE FROM `database`.customers
      WHERE first_name = "Test"
      ;  

```

### 8.	After testing, Joe thinks that there isn’t enough information in the transaction table.  Can you add another table to track transaction details (individual items in orders) and another to track the products the company sells?  

```sql

      CREATE TABLE  IF NOT EXISTS `order_details` (
      `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
      `transaction_id` INT(10) UNSIGNED NOT NULL,
      `product_id` INT(10) UNSIGNED NOT NULL,
      `qty_ordered` INT(10) UNSIGNED NOT NULL,
      PRIMARY KEY (`id`),
      FOREIGN KEY (`transaction_id` )
      REFERENCES `transactions` (`transaction_id` )
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
      FOREIGN KEY (`product_id` )
      REFERENCES `products` (`product_id` )
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
      )
      COLLATE='utf8_general_ci'
      ENGINE=InnoDB
      AUTO_INCREMENT=1
      ;

      CREATE TABLE IF NOT EXISTS `products` (
      `product_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
      `product_name` VARCHAR(45) NOT NULL,
      `product_description` TEXT NULL,
      `product_price` DECIMAL(19,4) NOT NULL,
      PRIMARY KEY (`product_id`) )
      COLLATE='utf8_general_ci'
      ENGINE=InnoDB
      AUTO_INCREMENT=1
      ;

```

### 9. Joe asks you to write a query that lets him see the total sales in dollars of all “Widget C” sales in the past 3 months.

```sql

      SELECT products.product_name, SUM(product_price * qty_ordered) AS total_sales FROM order_details
      INNER JOIN transactions
      ON order_details.transaction_id = transactions.transaction_id
      INNER JOIN products
      ON order_details.product_id = products.product_id
      WHERE products.product_id = 3
      AND transaction_date BETWEEN CURDATE() - INTERVAL 90 DAY AND NOW()
      ;

```

### 10. Joe asks you to write a query that lets him see all customers that ordered “Widget C” in the past 3 months ordered by Last Name, First Name.

```sql

      SELECT last_name, first_name FROM customers
      INNER JOIN transactions
      ON customers.customer_id = transactions.customer_id
      INNER JOIN order_details
      ON transactions.transaction_id = order_details.transaction_id
      WHERE transaction_date BETWEEN CURDATE() - INTERVAL 90 DAY AND NOW()
      AND order_details.product_id = 3
      ORDER BY 1
      ;

```

### 11. Joe wants you to think of any other fields that could be added to the tables to make the database more future proof.  He also wants you to review the final structure to make sure everything looks good.  Let him know if you would change anything else.

      * Add a 'transaction_type' (withdraw, deposit) field to transactions table to allow all transaction amount to be a positive integer (correct transaction query by amount > or < a certain figure).
      * Add a trigger to customer table to autmatically adjust account_balance after transactions are inserted into transaction table.
      * Would remove the 'delete' cascade on customer foriegn key so deleting a customer doesn't remove all transactions. Instead would remove test transactions through query.

### 12. Joe asks you to provide a relationship diagram of the final structure so he can review it with others.

![sql diagram](/diagrams/diagram.png)

## Create specifications with pseudo-code for classes you feel are necessary to return the data from the database in object format with relevant functions to handle the queries he requested before.

      #### Node/Express Server
```javascript

      // app.js
      const express = require('express');

      //routes import
      const routes = require('./routes/');

      // app
      const app = express();
      app.use('/api', routes);

      const port = process.env.PORT || 3001

      app.listen(port, function () {
      console.log(`Listening on port ${port}`);
      });

```

```javascript

      // database/index.js

      const mysql = require('mysql');

      //import user/password config
      const config = require('../config');

      const db = mysql.createConnection({ //may use pooled connection
      host: 'localhost',
      user: config.user,
      password: config.password,
      database: 'simulations'
      });

      db.connect((err) => {
      if(err) {
      console.log(err);
      }
      console.log(`connected to database!`);
      });

      module.exports = db;

```
```javascript

      //routes --includes sql queries
      const express = require('express');
      const router = express.Router();
      const mysql = require('mysql');

      //import database
      const db = require('../database/')

      //routes
      router.get('/allcustomers', (req, res) => {
      db.query (
      `SELECT * FROM customers`, (err, result) => {
            if (err) {
            res.send(500, 'Server Error', err)
            };
            res.json(result);
      });
      });

      module.exports = router;


```
```javascript

      //Query functions built to allow user defined variables, filters
      const customer = {firstName: 'John', lastName: 'Doe', age: 18, email: 'j.doe@gmail.com'} //map to input
      db.query (
        'INSERT INTO customers (first_name, last_name, age, email) VALUES ?', customer, (err, res) => {
          if (err) throw err;
          res.json('Last insert ID:', res.insertId);
      });

     //dynamic Select query all
      function buildQuery(selectArr, paramArr, orderArr ) {
        
        let queryFrame =  'SELECT ? FROM products JOIN order_details ON products.product_id = order_details.product_id JOIN transactions ON order_details.transaction_id = transactions.transaction_id JOIN customers ON transactions.customer_id = customers.customer_id ? ?';

        let selectFields = [], paramFields = [], orderFields = [];

        if (selectArr.length === 0) {
          selectFields.push('*');
        } else {
          selectFields = selectArr.join();
        }

        //need to break out paramFields to allow better WHERE clauses
        if (paramArr.length === 0) {
          paramFields = '';
        } else {
          paramFields = 'WHERE ' + paramArr.join(' AND ');
        }

        if (orderArr.length === 0) {
          orderFields = '';
        } else {
          orderFields = 'ORDER BY ' orderArr.join();
        }       

        db.query (queryFrame, selectFields, paramFields, orderFields, (err, res) => {
          if (err) throw err;
          res.json(rows);
        });
      }

```


## Joe wants you to design a prototype webpage where he could list all transactions and associated items for the past 3 months.  He said he may need to restrict transactions to a particular user.  He also insists on having a button at the top of the page that will allow him to print the page even though you know he can use the browser’s print feature.

      --Backend
            -server at /app.js --> 
                  --/routes/index.js for RESTful interface, connects to database (/database/index.js), hosts queries.
                  --/database/index.js uses /config.js to load connection info (user, password, can attach host, database); /config.js is gitignored.
      --Frontend
            --styles not added.
            -hosted in 'client' folder
                  --/src/index.js loads app
                  --/containers/App.js hosts TransactionsPage container
                  --/containers/TransactionsPage hosts app state, async api calls to server to gather data, print function.
                        --pass query data (state: transactions) to table component (/components/TransactionsTable.js)
                        --user data (state: customers) passed to FilterUser component (/components/FilterUser.js) with filter function
                              --input on FilterUser component calls route with condition on customer_id ('WHERE customer_id === x')
                                    --re-renders table filtered by user/customer

