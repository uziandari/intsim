const express = require('express');

//routes import
const routes = require('./routes/');

// app
const app = express();
app.use('/api', routes);

const port = process.env.PORT || 3001

app.listen(port, function () {
    console.log(`Listening on port ${port}`);
});
