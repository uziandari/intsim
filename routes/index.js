const express = require('express');
const router = express.Router();
const mysql = require('mysql');

//import database
const db = require('../database/')

//routes
router.get('/customers', (req, res) => {
  db.query (
    `SELECT customer_id, first_name, last_name FROM customers`, (err, result) => {
      if (err) {
        res.send(500, 'Server Error', err)
      };
      res.json(result);
  });
});

//get all
router.get('/transactions', (req, res) => {
  db.query (
    `SELECT * FROM products
    JOIN order_details
    ON products.product_id = order_details.product_id
    JOIN transactions
    ON order_details.transaction_id = transactions.transaction_id 
    JOIN customers
    ON transactions.customer_id = customers.customer_id
    WHERE transaction_date BETWEEN CURDATE() - INTERVAL 90 DAY AND NOW()`, (err, result) => {
      if (err) {
        res.send(500, 'Server Error', err)
      };
      res.json(result);
  });
});

//params --for filter by customer ID
router.get('/transactions/:custID', (req, res) => {
  db.query (
    `SELECT * FROM products
    JOIN order_details
    ON products.product_id = order_details.product_id
    JOIN transactions
    ON order_details.transaction_id = transactions.transaction_id 
    JOIN customers
    ON transactions.customer_id = customers.customer_id
    WHERE transaction_date BETWEEN CURDATE() - INTERVAL 90 DAY AND NOW()
    AND customers.customer_id = ${req.params.custID}`, (err, result) => {
      if (err) {
        res.send(500, 'Server Error', err)
      };
      res.json(result);
  });
});

module.exports = router;
