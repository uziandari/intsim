const mysql = require('mysql');

//import user/password config
const config = require('../config');

const db = mysql.createConnection({ //may use pooled connection
  host: 'localhost', 
  user: config.user,
  password: config.password,
  database: 'simulations' 
});

db.connect((err) => {
  if(err) {
    console.log(err);
  }
  console.log(`connected to database!`);
});

module.exports = db;
